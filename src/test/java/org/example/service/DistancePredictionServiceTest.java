package org.example.service;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DistancePredictionServiceTest {
  @Test
  void shouldPredict() {
    // (A)rrange
    final DistancePredictionService service = new DistancePredictionService();
    final float volume = 25.0f;
    final float consumption = 7.9f;
    final int expected = 316;

    // (A)ct
    final int actual = service.predict(volume, consumption);

    // (A)ssert
    assertEquals(expected, actual);
  }
}
