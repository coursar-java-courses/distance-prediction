package org.example;

import org.example.service.DistancePredictionService;

public class Main {
  public static void main(String[] args) {
    final DistancePredictionService service = new DistancePredictionService();
    final int prediction = service.predict(25, 10);
    System.out.println(prediction);
  }
}
