package org.example.service;

public class DistancePredictionService {
  public int predict(float volume, float consumptionPer100Km) {
    final float result = volume / consumptionPer100Km * 100;
    return (int) result;
  }
}
